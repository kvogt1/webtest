<img align="right" src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" width="100" height="100" border="0">
<br><br><br><br><br>

## *Exhibit A* – Statement of Work

|                              |                     |
| ----                         | --------            |
| PO #                         | 8675309                 |
| Customer's Name              | Acme          |
| Customer Billing Information | 321 Fake St Rochester NY, 14586  |
| Contract Infrormation        | 789789 |
| VAT ID                       | 7             |
| Implementation Engineer      | Kevin Vogt           |
| Requested by                 | John Smith, Acme Corp       |
| From                         | David Tacheny, Sales Exec               |

**1.Service Description**

GitLab, Inc. (“GitLab”) will provide Training for several Acme team members

**2.Project Scope**

The scope of this engagement:

We will provide admin and CICD training

In performance of the Services herein, GitLab shall perform the project activities listed in this exhibit (each an “Activity”)  for the purpose of providing the following Deliverables, as more fully defined below.



<span id="anchor-3"></span>A.Activities: 

Training, Consulting

We will provide 5 days remote training

| Task   | Duration        |
| ----   | --------        |
| $TASK  | $TASK _DURATION |

This schedule is only an estimate and may be subject to change. All durations in this exhibit are assumed on accounting for the business days. Estimates provided in this document are based on rough estimation of project effort to be performed by GitLab’s  Trainers, Consultants, Certain activities (DD Training, Consulting) may take more or less time, and resources depending on the actual need as may be specified by the Customer. GitLab welcomes the opportunity to confirm these assumptions with you, and obtain more information about your expectations for this project. GitLab will be pleased to provide a revised proposal in a better position accordingly. 

**B.Deliverables:**

| Deliverable  | 
| ----         | 
| $DELIVERABLE | 

C.GitLab’s Obligations: GitLab shall provide the customer following materials for all delivered 

  - > $OBLIGATIONS

**D.Customer’s Obligations:**

  - > Provide timely access to office accommodations, facilities, equipment, assistance, cooperation, complete and accurate information and data to the extent necessary to provide a beneficial and effective training (collectively, “cooperation”) are essential to the performance of any Services as set forth in this exhibit. 

  - > GitLab will not be responsible for any deficiency in performing training services if such deficiency results from the Customer’s failure to provide full cooperation. 

  - > Acme acknowledges that GitLab’s ability to perform the training services and any related estimate depends on the Customer’s fulfillment of the obligations and the project assumptions outlined in this exhibit A. 

  - > Provide a safe and sound workspace (i.e., free from any hazards that are known or likely can cause death or serious physical harm, pose serious health risks, and otherwise reasonably acceptable space for performing similar professional services, etc.). 

  - > During the performance of services, if GitLab requires access to other third-party products that are part of Acme system, Acme will be responsible for acquiring all such products and any applicable license rights as may be necessary for GitLab to access such products on Acme behalf.

**3.Rates, Estimated Fees and Payment:**

The services specified above are provided on a \[time and materials (“T\&M”)/ fixed price (“FP”)\] basis. The Customer shall pay GitLab for all of the time spent performing such services, plus materials, taxes and expenses. All costs listed below are based on the scope and assumptions included in this Statement of Work, and will become due in full after completion of the deliverables. The estimated fee for the service under this exhibit is Seven thousand dollars. Below is the breakdown of the pricing:

| Item  | Cost  |
| ----- | ----  |
| $ITEM | $COST |

The above-stated pricing is based on the scope and assumptions set forth herein. In the event of a change of scope or assumptions, this pricing is subject to change. All fees and charges listed above shall be invoiced upon execution of this SOW. Travel related expenses shall be invoiced at the completion of the project.

Invoices shall be due and payable net thirty (30) days from the invoice date. All invoices shall be provided to the Customer’s Billing Information contact listed above. Customer shall inform GitLab promptly in writing if there is any change to the information where the invoices should be sent. The customer $IS_PO_BOOL require a Purchase Order (“P.O.”) in order to make payments to GitLab for all fees and expenses related to this project, as outlined in this SOW. If neither box is checked, Customer agrees that Gitlab is entitled to payment of invoices without the requirement of a P.O. authorizing payments. In the event that Customer checks the box indicating that a P.O. is required, then the Customer agrees to provide required P.O. to GitLab within ten (10) business days after the execution of the SOW by the Customer.

The customer is responsible for travel-related expenses incurred for the purpose of this engagement. If meetings or work are scheduled at the Customer premises, all reasonable travel, meals and living expenses shall be billable at the United States government rate and all such expenses shall be borne solely by Customer. Expenses for materials purchased specifically for Customer’s benefit, if any, will be subject to prior written approval and charged to Customer at cost. Gitlab’s policy for sponsored travel can be found at this URL: [*https://about.gitlab.com/handbook/travel-sponsored/*](https://about.gitlab.com/handbook/travel-sponsored/)

The fees set forth on this SOW are exclusive of all taxes, levies, or duties imposed by taxing authorities and the Customer shall be responsible for payment of any such taxes, levies or duties, excluding only taxes based solely on GitLab’s net income. Accurate sales tax will be added to invoices when applicable. 

**4.Project Management:**

Acme and GitLab each agree to designate a project manager who
shall work together with the other party’s designated project manager to
facilitate efficient delivery of services. 

**5.Project Assumptions:**

  - > $PROJECT_ASSUMPTIONS

  - > Any activity or deliverable not expressly stated herein shall be deemed out of scope for purposes of this SOW, and is not included in the pricing set forth below.

  - > Access provisioning and deprovisioning to customer’s environments will be handled according to customer’s access control policies.

  - > All accounts provisioned should be named, i.e. no shared accounts.

**6.Acceptance**
<br><br>
The parties hereto, duly represented by an authorized signatory, for and behalf of the business entity it represents, hereby agree to and accept
the terms set forth in this SOW, which is governed by the GitLab Service Terms and can be accessed at [*https://about.gitlab.com/terms/\#consultancy*](https://about.gitlab.com/terms/#consultancy) and are fully incorporated by reference herein. 
<br><br>


| GitLab Inc.<br> __________________________________________________     | Acme <br> __________________________________________________      |
|                            ------------------                          |                           ------------------                            |
| <br>__________________________________________________<br> Full name   | <br>__________________________________________________<br> Full name    |
| <br>__________________________________________________<br> Title       | <br>__________________________________________________<br> Title        |
| <br>__________________________________________________<br> Signature   | <br>__________________________________________________<br> Signature    |
| <br>__________________________________________________<br> Date        | <br>__________________________________________________<br> Full name    |
| <br>__________________________________________________<br> Full name   | <br>__________________________________________________<br> Full name    |
| <br>__________________________________________________<br> Full name   | <br>__________________________________________________<br> Full name    |
