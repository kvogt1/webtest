import argparse, sys
import fileinput

# python3 createsow.py --PO 8675309 --cust_name "Acme" --cust_billing_info "321 Fake St Rochester NY, 14586" --cust_contract_info 789789 --vat_id 7 --engineer "Kevin Vogt" --requested_by "John Smith, Acme Corp" --sales_info "David Tacheny, Sales Exec" --service_description "Training for several Acme team members" --project_scope "We will provide admin and CICD training" --schedule "We will provide 5 days remote training" --professionals "Trainers, Consultants" --activities "DD Training, Consulting" --cost_written "Seven thousand dollars" --activity "Training, Consulting" --task_list "CI/CD Training, Architecture Review" --task_duration_list "1 Day, 1 Day" --deliverable_list "1 Day CI/CD Training,1 Day Architecture Review" --obligations_list "1 Day On Site Training for CI/CD Performed by Gitlab Trainer,1 Day On Site Architecture Review performed by Gitlab Consultant" --project_assumptions_list "Customer Will Provide Lunch, Customer will provide coffee" --item_list "1 Day CI/CD Training,1 Day Architecture Review" --cost_list "3000,4000"
print("Starting SOW Generation")
sow_template = './sow_template.md'
# Arguments
parser = argparse.ArgumentParser()
parser.add_argument('--PO', help = 'PO Number From Customer', default="")
parser.add_argument('--cust_name', help = 'Customer Name', default="")
parser.add_argument('--cust_billing_info', help = 'Customer Billing Address', default="")
parser.add_argument('--cust_contract_info', help = 'Contract Number', default="")
parser.add_argument('--vat_id', help = 'VAT ID', default="")
parser.add_argument('--engineer', help = 'The Engineer that will be Assigned to the SOW', default="")
parser.add_argument('--requested_by', help = 'CUstomer that Requested SOW', default="")
parser.add_argument('--sales_info', help = 'Sales Exec Fillin in SOW', default="")
parser.add_argument('--service_description', help = 'Description of the Service', default="")
parser.add_argument('--project_scope', help = 'Scope of the Project', default="")
parser.add_argument('--activity', help = 'Text Description of Activited to be Performed', default="")
parser.add_argument('--schedule', help = 'Text Description of the Schedule', default="")
parser.add_argument('--task_list', help = 'Comma Seperated List of Items to be Performed', default="")
parser.add_argument('--task_duration_list', help = 'Correlating Comma Seperated List of Duration to Match Taks', default="")
parser.add_argument('--professionals', help = 'Insert One or Multiples (Trainers, Consultants, Profectional Resources)', default="")
parser.add_argument('--activities', help = 'Details on the Activities to be provided', default="")
parser.add_argument('--deliverable_list', help = 'Comma Seperated List of Deliverables', default="")
parser.add_argument('--obligations_list', help = 'Comma Seperated List of Obligations', default="")
parser.add_argument('--cost_written', help = 'Written Cost (ex: five-hundred dollars', default="")
parser.add_argument('--item_list', help = 'Comma Seperated List of Chargeable Items', default="")
parser.add_argument('--cost_list', help = 'Comma Seperated List of Charges Correlating to Items', default="")
parser.add_argument('--project_assumptions_list', help = 'Comma Seperated list of Project Assumptions', default="")

args = parser.parse_args()

new_sow_path = "./" + args.cust_name + ".md"
new_sow_md = ""

find_replace = {
    "$PO": args.PO,
    "$CUST_NAME": args.cust_name,
    "$CUST_BILLING_INFO": args.cust_billing_info,
    "$CUST_CONTRACT_INFO": args.cust_contract_info,
    "$VAT_ID": args.vat_id,
    "$ENGINEER": args.engineer,
    "$REQUESTED_BY": args.requested_by,
    "$FROM": args.sales_info,
    "$SERVICE_DESCRIPTION": args.service_description,
    "$PROJECT_SCOPE": args.project_scope,
    "$ACTIVITY": args.activity,
    "$SCHEDULE": args.schedule,    
    "$PROFESSIONALS": args.professionals,
    "$ACTIVITIES": args.activities,
    "$COST_WRITTEN": args.cost_written
}

def build_table(col1, col2):
    col1 = col1.split(',')
    col2 = col2.split(',')
    tbl = ""
    for num, row in enumerate(col1):
        tbl += "|%s|%s|\n" % (col1[num], col2[num])
    return(tbl)

def build_list(lst_items):
    return("  - > " + lst_items.replace(",", "\r\n  - > " ))

with open(sow_template) as data:
    with open('new_data.txt', 'r') as new_data:
        for line in data:
            # Replace Dictionary Matches
            for key in find_replace:
                if key in line:
                    line = line.replace(key, find_replace[key])
            # Set PO Boolean
            if "$IS_PO_BOOL" in line:
                if args.PO == "":
                    line = line.replace("$IS_PO_BOOL", "does not" )
                else:
                    line = line.replace("$IS_PO_BOOL", "does" )

            # Create Task List
            if "$TASK" in line:
                line = line.replace("$TASK", build_table(args.task_list, args.task_duration_list))
            # Create Cost List
            if "$COST" in line:
                line = line.replace("$COST", build_table(args.item_list, args.cost_list))

            # Create Deliverables List
            if "$DELIVERABLE" in line:
                line = line.replace("$DELIVERABLE", build_list(args.deliverable_list))
            # Create Obligations List
            if "$OBLIGATIONS" in line:
                line = line.replace("$OBLIGATIONS", build_list(args.obligations_list))
            # Create Cost List
            if "$PROJECT_ASSUMPTIONS" in line:
                line = line.replace("$PROJECT_ASSUMPTIONS", build_list(args.obligations_list))
            # Create Project Assumptions List
            new_sow_md += line

file1 = open(new_sow_path,"w")
file1.write(new_sow_md) 
file1.close()

print("SOW Generation Complete")

